FROM openjdk:8-jre-alpine3.9

ARG PMD_VERSION=${PMD_VERSION:-7.1.0}

RUN mkdir -p /opt

RUN cd /opt \
      && apk add --no-cache --virtual .build-deps wget unzip \
      && wget -nc -O pmd.zip https://github.com/pmd/pmd/releases/download/pmd_releases/${PMD_VERSION}/pmd-dist-${PMD_VERSION}-bin.zip \
      && unzip pmd.zip \
      && rm -f pmd.zip \
      && ls -la \
      && mv pmd-bin-${PMD_VERSION} pmd \
      && apk del .build-deps

RUN apk add --update --no-cache dumb-init

ENTRYPOINT [ "/usr/bin/dumb-init", "/usr/bin/java", "-classpath", "/opt/pmd/lib/*", "net.sourceforge.pmd.PMD" ]
CMD ""
